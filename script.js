console.log("Hello World");

// Create a "trainer" object by using object literals.
let trainer = {
	name: "Ash",
	pokemon: "Pickachu",
	age: 10,
	catch: function(){
		console.log("Pokemon caught");
	}
};
//Access the "trainer" object properties using dot and square bracket notation.
console.log(trainer.name);
console.log(trainer['pokemon']);
//Invoke/call the "trainer" object method.
console.log(trainer.catch());

// Create a constructor function for creating a pokemon.
function Pokemon(name){
	this.name = name;
	this.health = 100;
	this.tackle = function(target){
		console.log(`${this.name} attacked ${target.name}`);
		target.health -= 10;
		console.log(`${this.name}'s health is now ${target.health}`)
	}
};

let jigglypuff = new Pokemon("Jigglypuff");
let blastoise = new Pokemon("Blastoise");

//Have the pokemon objects interact with each other by calling on the "tackle" method.
jigglypuff.tackle(blastoise);